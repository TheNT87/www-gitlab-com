---
layout: markdown_page
title: "On-boarding process for Premium Support"
---

# On-boarding process for Premium Support
After purchase of a Premium Support subscription is completed, the following steps will take place:

- Sales verifies that the all the necessary contact information exists in the SalesForce record for the customer
   - Email address of the primary contact person is mandatory
   - Domain name where email requests will come from is required for proper Zendesk routing
- Sales sends the customer an email message with the [text contained here](premium_support_message.txt) so the customer knows what to expect.
- Sales opens a [confdential issue in the support project](https://gitlab.com/gitlab-com/support/issues)
   - Apply the ["Dedicated service engineer"](https://gitlab.com/gitlab-com/support/blob/master/.gitlab/issue_templates/Dedicated%20service%20engineer.md) issue template 
   - Assign the issue to the "Support Lead" 
- Sales notifies the designated Support person who processes the issue.
- Support assigns an Service Engineer based on a number of factors
   - Location
   - Availability
   - SLA
- Assigned support engineer contacts customer and provides the following via email / telephone as appropriate:
   - Personalized introduction
   - Contact information for support subscribers
   - Information on 24x7 emergency support
   - Scheduling for two training workshops
   - Discussion (and possible scheduling) of live upgrade assistance
