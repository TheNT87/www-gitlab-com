---
layout: markdown_page
title: "GitLab Onboarding 101"
---

## Other pages

* [Sales onboarding](/handbook/sales-onboarding)
* [Developer onboarding](/handbook/developer-onboarding)
* [UX Designer onboarding](/handbook/uxdesigner-onboarding)
* [UX Researcher onboarding](/handbook/uxresearcher-onboarding)
* [Designer (marketing) onboarding](/handbook/designer-onboarding)
* [Service engineer onboarding](/handbook/support/onboarding/)
* [Offboarding](/handbook/offboarding/)
* [Glossary](/handbook/glossary/)
* [General Onboarding](/handbook/general-onboarding)

----

## On this page
{:.no_toc}

- TOC
{:toc}

----

## Welcome to GitLab

Congratulations on completing the interview process, and starting your first day at GitLab. We are so excited that you are here! This page is designed to help you navigate the onboarding process from reading the handbook, to completing the onboarding issue. Regardless of your role, we all use GitLab, and if you come from a non-technical background, this might be new to you! At times the onboarding issue might feel overwhelming, but if you have any additional questions along the way, please feel free to ask [People Ops](/handbook/people-operations/), your buddy, or anyone else at GitLab in the `#questions` channel on Slack.

## Your First Day

Throughout your first few days at GitLab it is important to focus on the onboarding issue; but where to start! This guide will help you understand what is in the onboarding issue, and why we are asking you to complete the items on the issue.

### Getting Started

* Review our [Team Call Section](/handbook/#team-call) for the daily call. In the calendar invite is a link to the Team Call agenda. This holds what we will be talking about that day, as well as the order of Team Monday-Thursday at the top of that page. If you are curious when you will get to share what you have been up to, you can look there.


* Connect with GitLab's social media sites:
  1. [LinkedIn](https://www.linkedin.com/company/gitlab-com)
  1. [Twitter](https://twitter.com/gitlab)
  1. [Facebook](https://www.facebook.com/gitlab)
  1. [YouTube](https://www.youtube.com/channel/UCnMGQ8QHMAnVIsI3xJrihhg)

### Working Remotely

1. The first month at a remote company can be hard. Take a look at [3 Things I Learned in My First Month at GitLab](https://about.gitlab.com/2016/11/02/three-things-I-learned-in-my-first-month-at-gitlab/) for some insight from a GitLabber.

## The Onboarding Issue

In the onboarding issue, we ask you to complete a variety of tasks. Here are some [tips](https://about.gitlab.com/handbook/#tools) on items like gmail, zoom, calendly, etc.

### 1Password

We recommend that you set up your 1Password account early in the onboarding process since you will need it to hold all of the new passwords that you will create. This is an awesome tool, that GitLab uses to store all passwords for each department. You will also have your personal vault where you can store all the passwords that you are creating for GitLab. In keeping with our [security best practices](https://about.gitlab.com/handbook/security/). We ask that you do set up this 1Password account with your GitLab email, even though you might have one already.

### 2FA

GitLab asks you to enable 2FA (2-Step Verification, also known as two-factor authentication), because it adds an extra layer of security to your account. You sign in with something you know (your password) and something you have (a code sent to your phone). If you have problem with 2FA just let people ops know! We can disable it for you to then set it up again. If the problem persists, then we can direct you to one of our super admins.

### BambooHR and TriNet

BambooHR is our HRIS (Human Resource Information System) for all team members. We have self-service enabled so that at any time you can access your account to see your employment information and documentation. As part of onboarding, please make sure to update everything that is applicable to you on the Personal tab, your ethnicity (which is located on the Jobs tab), and emergency contact information. We also ask that you provide People Ops with a Photo ID which is used for Employment Verification Purposes. You can send this documentation in any way you feel most comfortable. Some common ways in the past have been emailing People Ops, sending as a direct message to our People Ops Specialist in Slack, or scheduling a call with our People Ops Specialist to view the documentation.

The information from BambooHR is used to feed other systems, so it is important that it is filled out before you start. If you are a GitLab Inc. employee, you will receive login information from TriNet, our payroll and benefits provider, within 24 hours of your start date.

#### GitLab.com vs Dev.GitLab.org

Your onboarding issue was created on the dev server, which is only viewable to team members. Per the [gitlab workflow](https://about.gitlab.com/handbook/#gitlab-workflow) point 9, everything that can be public should be out in the open. GitLab.com is viewable to anyone with an account. We ask you to set up your GitLab.com account with your GitLab email for additional security.

#### Calendly

Calendly is a calendar tool that allows individuals to select open meeting slots in order to speak with others at the company. (Great for also setting up coffee breaks!) All team members add Calendly to their Slack profile as a part of onboarding, so if you ever need or want to speak with another team member, you can schedule the meeting yourself. When you are setting up Calendly, there is no specific GitLab account. With the free version you will only be allowed one meeting time, but if you need to upgrade to a Pro account, you can do so and expense it per [spending company money](https://about.gitlab.com/handbook/#spending-company-money).

#### Egencia

You will receive an email to set up your account for Egencia. This travel is for company purposes, and is paid for by the company at the time of booking. For more information on setting up your account please visit our [travel page](https://about.gitlab.com/handbook/travel/#booking-travel-and-lodging-with-egencia).

### Add yourself to the team page

We are happy to have you join our company and to include
you in our [team page](https://about.gitlab.com/team/). The following are
the steps to add yourself to the team page. Please follow the links that will guide you to full guides about every step. If you are not familiar with local git yet, feel free to do this through the web interface, and ask anyone at the company questions along the way.

#### Add on GitLab.com

1. As part of your onboarding issue, you created an account on [GitLab.com](https://gitlab.com/). take note of your username and password, because you will need them throughout these steps.
1. You should have been granted access to the [www-gitlab-com project](https://gitlab.com/gitlab-com/www-gitlab-com) as an earlier step of onboarding.
1. You need to follow the [GitLab Workflow](https://about.gitlab.com/2016/10/25/gitlab-workflow-an-overview/) to complete this task.
1. Find the picture that you’d like to add to
our [team page](https://about.gitlab.com/team/), change the picture's name to
the following format: `yourname.jpg` or `yourname.png` and then follow the
"[how to add an image](http://doc.gitlab.com/ce/gitlab-basics/add-image.html)" steps. Picture Notes:
   - Ensure the picture size is around 400x400 (it must be square) and the format is JPEG or PNG. You can resize your picture using a photo editor like [GIMP](http://www.gimp.org/) (cross-platform) or online by searching for "image resize".
   - Any picture that you provide will be made black-and-white automatically after you add it.
1. In [GitLab.com](https://gitlab.com/), select the GitLab.com/www-gitlab-com project.
1. Click on "Files".
1. Select Source, Images, then Team.
1. At the top of the page select “+” and upload the file. Note that your team page picture should be added to `www-gitlab-com/source/images/team/NAME-OF-PERSON-IN-LOWERCASE.jpg`.
1. Commit these changes with a specific commit message “add NAME to team page” and a unique branch. Remember the branch name as you will use it in the following steps.
1. [Create a Merge Request](http://doc.gitlab.com/ce/gitlab-basics/add-merge-request.html) in [GitLab.com](https://gitlab.com/) with the branch that you created with your picture.
1. Find the dropdown menu at the top of your screen and find the branch
that you previously created to add your picture (they are in alphabetical
order).
1. Go back to the GitLab.com project. Information displayed on [Team page](https://about.gitlab.com/team/) is
pulled from a data file. You can find it by clicking on each of the following items: Files, `data/`, and then `team.yml`.
1. When you are in `team.yml`, click on “edit” on the top right side of
your screen.
1. Your information should already be added after the last person on the team page. Update the initials to be your `Firstname 'Nickname' Lastname`. Verify that your title is entered correctly. Add the filename of the picture that you uploaded previously. Enter your twitter and gitlab handle. Write a story about yourself.
Don't forget to use other team members' information as a reference and to
respect the spaces between lines. Please don't use "tab" because it will break the page format.
1. After you added your information, add a comment to your commit and click on “Commit Changes”.
1. Go to the Merge Request that you previously created with the branch that you are using and assign it to your manager for review.

#### Add Locally

1. As part of your onboarding issue, you created an account on [GitLab.com](https://gitlab.com/). take note of your username and password, because you will need them throughout these steps.
1. You should have been granted access to the [www-gitlab-com
project](https://gitlab.com/gitlab-com/www-gitlab-com) as an earlier step of onboarding.
1. You need to follow the [GitLab Workflow](https://about.gitlab.com/2016/10/25/gitlab-workflow-an-overview/) to complete this task.
1. Download Git, following the [start using git
documentation](http://doc.gitlab.com/ce/gitlab-basics/start-using-git.html). Don't forget to add your Git username and to set your email.
1. Follow the steps to create and add your [SSH
keys](http://doc.gitlab.com/ce/gitlab-basics/create-your-ssh-keys.html).<br>
Note: in some of these steps, your
[shell](http://doc.gitlab.com/ce/gitlab-basics/start-using-git.html) will
require you to add your GitLab.com username and password.
1. Clone the www-gitlab-com project through your shell, following the [command line commands
documentation](http://doc.gitlab.com/ce/gitlab-basics/command-line-commands.html).
1. Find the picture that you’d like to add to
our [team page](https://about.gitlab.com/team/), change the picture's name to
the following format: `yourname.jpg` or `yourname.png` and then follow the
"[how to add an image](http://doc.gitlab.com/ce/gitlab-basics/add-image.html)" steps. Picture Notes:
   - Ensure the picture size is around 400x400 (it must be square) and the format is JPEG or PNG. You can resize your picture using a photo editor like [GIMP](http://www.gimp.org/) (cross-platform) or online by searching for "image resize".
   - Any picture that you provide will be made black-and-white automatically after you add it. Note that your team page picture should be added to `www-gitlab-com/source/images/team/NAME-OF-PERSON-IN-LOWERCASE.jpg`.
1. [Create a Merge Request](http://doc.gitlab.com/ce/gitlab-basics/add-merge-request.html) in [GitLab.com](https://gitlab.com/) with the branch that you created with your picture.
1. In [GitLab.com](https://gitlab.com/), click on "Files".
1. Find the dropdown menu at the top of your screen and find the branch
that you previously created to add your picture (they are in alphabetical
order). If you don't have a branch yet, follow the steps to [create a new
branch](http://doc.gitlab.com/ce/gitlab-basics/create-branch.html).
![dropdown menu](/images/dropdown-branch-teampage.jpg)
1. Information displayed on [Team page](https://about.gitlab.com/team/) is
pulled from a data file. You can find it by clicking on each of the following items: `data/` and then `team.yml`.
1. When you are in `team.yml`, click on “edit” on the top right side of
your screen.
1. Your information should already be added after the last person on the team page. Update the initials to be your `Firstname 'Nickname' Lastname`. Verify that your title is entered correctly. Add the file name of the picture that you uploaded previously. Enter your twitter and gitlab handle. Write a story about yourself.
Don't forget to use other team members' information as a reference and to
respect the spaces between lines. Please don't use "tab" because it will break the page format.
1. After you added your information, add a comment to your commit and click on “Commit Changes”.
1. Go to the Merge Request that you previously created with the branch that you are using and assign it to your manager for review.

## Reading the Handbook

A core value of GitLab is documentation. Therefore, everything that we do, we have documented on about.gitLab.com. This can make the handbook seem huge! Don't let it scare you. To simplify navigating the handbook here are some suggested steps. Feel free to take a wrong turn at any time to learn more about whatever you are interested in.

1. Read [about](https://about.gitlab.com/about/) the company, and [How we use GitLab to build GitLab](https://about.gitlab.com/2015/07/07/how-we-use-gitlab-to-build-gitlab/). It is important to understand our [culture](/culture), and how the organization was started. If you have any questions about company products you can always check out our [features](https://about.gitlab.com/features/#compare) and [products](https://about.gitlab.com/products/).
1. Tackle the main page of the [handbook](/handbook/). You can use this link and read through the entire page to ensure that you do not miss anything! Here are some highlights that are important for new hires to pay attention to:
  * Make sure to read over our [values](https://about.gitlab.com/handbook/#values) and [general guidelines](https://about.gitlab.com/handbook/#general-guidelines) to answer any questions you might have about what to do and when.
  * If you have questions about what is in the handbook check out the [handbook usage](https://about.gitlab.com/handbook/#handbook-usage) as well as your own department's sub-page of the handbook (which can be found at the top of the handbook).
  * [Communication](https://about.gitlab.com/handbook/#communication) is extremely important within a remote only organization. Read through the guidelines to understand how we make [working remotely](https://about.gitlab.com/handbook/#working-remotely) successful.
  * Don't forget to comply with the contract you signed, and make sure you understand [Intellectual Property](https://about.gitlab.com/handbook/#intellectual-property).
1. Since we are a global organization understand your benefits might be different than other team members at the company based on which [contract](https://about.gitlab.com/handbook/contracts/) you signed. It is important to understand **your** [benefits](https://about.gitlab.com/handbook/benefits/). If you have questions please reach out to People Ops.
  * Notable Benefits that apply to all team members:
    1. [Unlimited Time Off Policy](https://about.gitlab.com/handbook/#paid-time-off): GitLab truly does value a work-life balance, and encourages team members to have a flexible schedule and take vacations. If you feel uncomfortable about taking time off, or are not sure how much time to take off throughout the year, feel free to speak with your manager or People Operations. We will be happy to reinforce this policy! Please note the additional steps that might need to be taken if you are scheduled for [on call](https://about.gitlab.com/handbook/#on-call).
    1. Check out [People Operations](https://about.gitlab.com/handbook/people-operations/) to learn more about our [policies](https://about.gitlab.com/handbook/people-operations/#policies).
    1. As part of onboarding, you received an email asking if you needed any equipment. Throughout your time at GitLab you might need additional equipment, would like to learn how to code, or want to visit a team member! Make sure you know what you can [spend company money](https://about.gitlab.com/handbook/#spending-company-money) on.
    1. The handbook also describes [incentives](https://about.gitlab.com/handbook/#incentives) such as sales, dinners, or bonuses, that can apply to all GitLab team members. Know any great talent to refer?
1. Even if you come from a technical background, it is important to understand the [proper workflow](https://about.gitlab.com/handbook/#gitlab-workflow) since this is how we are all able to operate remotely in a successful way. If you come from a non-technical background, and would like more clarification on what this workflow means, feel free to ask People Ops, your manager, or anyone else at the company.

### Learning Git and GitLab

If you need any help with Git itself, subscribe to the `#git-help` channel on Slack and feel free to ask any question you might have.

### 60-day Onboarding Survey

To help PeopleOps understand your experience and improve the onboarding process, please complete the 60-day [onboarding survey] (https://docs.google.com/a/gitlab.com/forms/d/e/1FAIpQLSdU8qxThxRu3tdrExFGBJ1GlyucCoJj2kDpcvOlM4AiRGOavQ/viewform) after you have been working for GitLab for more than 2 months! PeopleOps will send monthly reminders to team members to complete the survey!

#### Becoming Familiar with Projects on GitLab.com

* You should have been provided access to our [Internal GitLab Server](https://dev.gitlab.org). Once you feel comfortable with GitLab, take a moment to familiarize yourself with:
  1. The Dashboard
  1. The Projects
  1. The Issue Tracker
* Become familiar with the README.md’s for these projects:
  1. [GitLab Enterprise Edition](https://gitlab.com/gitlab-org/gitlab-ee/)
  1. [GitLab Community Edition](https://gitlab.com/gitlab-org/gitlab-ce/)
  1. [GitLab www-gitlab-com](https://gitlab.com/gitlab-com/www-gitlab-com)
* As part of onboarding you created an account on our external / public [GitLab Server](https://gitlab.com). Have your manager grant access to the GitLab Enterprise Edition Project, Standard Subscribers Group and other projects / groups relevant to your role.
* Learn how to use our Internal Issue Trackers:
We use GitLab Issues to raise awareness, discuss, and propose solutions for various issues related to any aspect of our business.
The most common Issues are created in the following projects:
  1. [GitLab Enterprise Edition](https://gitlab.com/gitlab-org/gitlab-ee/issues) - Issues related to GitLab Enterprise Edition
  1. [GitLab Community Edition](https://gitlab.com/gitlab-org/gitlab-ce/issues) - customer requests
  1. [GitLab www-gitlab-com](https://dev.gitlab.org/gitlab/www-gitlab-com) - Issues related to our website
* Add issues in the correct Issue Tracker:
  1. Public issues related to GitLab website: use [www-gitlab-com](https://gitlab.com/gitlab-com/www-gitlab-com)
  1. Internal issues related to documentation and gitlab.com usage: Use [GitLab HQ](https://dev.gitlab.org/gitlab/gitlabhq)
  1. Issues related to the organization: Use [GitLab Organization](https://gitlab.com/gitlab-com/organization) consider making them confidential
  1. Internal issues relate to Enterprise Edition: Use [GitLab EE](https://dev.gitlab.org/gitlab/gitlab-ee)

#### Learn how to use Git Locally

Learn how to [get started with git](https://about.gitlab.com/handbook/#starting-with-git). It is important for all team members to understand how to work on the Web UI and locally. The rest of the team is happy to assist in teaching you git.

## Questions?

At any time if you have questions, please ask your manager, buddy, People Ops, or anyone else at the company!


Please add to me. Questions that were not answered? Something that is not clear? Please create an issue or a merge request and assign it to People Ops to make this documentation better for the next new team member.
